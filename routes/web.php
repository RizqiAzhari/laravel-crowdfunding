<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::get('/route-1', 'VerifiedUserController@checkVerifiedUser')->middleware('verified_user');

Route::get('/route-2', 'AdminController@checkAdmin')->middleware('verified_user', 'admin');

Route::get('coba', function(){
//     $details['email'] = 'akhmadrizqi@gmail.com';
//     dispatch(new App\Jobs\SendOtpEmail($details));
// ​
//     dd('Berhasil');
});