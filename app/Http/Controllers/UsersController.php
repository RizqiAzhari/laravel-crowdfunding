<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class UsersController extends Controller
{
    public function getAllUsers() {
        $users = Users::get()->toJson(JSON_PRETTY_PRINT);
        return response($users, 200);
    }

    public function getUser($id) {
        if (Users::where('id', $id)->exists()) {
          $user = Users::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
          return response($user, 200);
        } else {
          return response()->json([
            "message" => "User not found"
          ], 404);
        }
      }

      public function createUser(Request $request) {
        $user = new Users;
        $user->name_user = $request->name_user;
        $user->email_verified_at = $request->email_verified_at;
        $user->save();
  
        return response()->json([
          "message" => "User record created"
        ], 201);
      }

      public function updateUser(Request $request, $id) {
        if (Users::where('id', $id)->exists()) {
          $user = Users::find($id);
  
          $user->name_user = is_null($request->name_user) ? $user->name_user : $user->name_user;
          $user->email_verified_at = is_null($request->email_verified_at) ? $user->email_verified_at : $user->email_verified_at;
          $user->save();
  
          return response()->json([
            "message" => "records updated successfully"
          ], 200);
        } else {
          return response()->json([
            "message" => "Book not found"
          ], 404);
        }
      }

      public function deleteUser ($id) {
        if(Users::where('id', $id)->exists()) {
          $user = Users::find($id);
          $user->delete();
  
          return response()->json([
            "message" => "records deleted"
          ], 202);
        } else {
          return response()->json([
            "message" => "Book not found"
          ], 404);
        }
      }
}
