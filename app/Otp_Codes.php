<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp_Codes extends Model
{
    protected $fillable = [
        'num_code', 'expired_date'
    ];
}
